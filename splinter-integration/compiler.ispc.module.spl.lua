-- ISPC compiler module.
-- Include this whenever you want to use the ISPC compiler.

spl_module({
    description="CFC Shader Compiler Resolver",
    action="compiler.ispc.lua"
})