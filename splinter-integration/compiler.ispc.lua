local action = {}

function action.init(state)
    if(settings.getSetting("build.ispc.build", "true") == "false") then return end

    log_dbg("ISPC: Initializing..")

    -- create new state
    state.ispccompiler = {}
    state.ispccompiler.custom_flags = settings.getSetting("cfc.build.ispc.args" , "--opt=fast-math")

    -- set compiler to windows
    state.ispccompiler.path_binary = utils.path_rel(scripts.get_build_dir(), utils.path_merge(state.ro_modules["compiler.ispc"].path, "../bin/ispc.exe"))
    state.ispccompiler.builtin_flags = "--target-os=windows "

    -- inject extra args
    if(scripts.spl_get_variable_bool("build.env.symbols", true) == true) then 
        state.ispccompiler.builtin_flags = state.ispccompiler.builtin_flags .. "-g " 
    end
    state.ispccompiler.builtin_flags = state.ispccompiler.builtin_flags .. "-O" .. scripts.spl_get_variable("build.ispc.optimization.level", "2") .. " "
    if scripts.spl_get_variable("build.ispc.target", "") ~= "" then
        state.ispccompiler.builtin_flags = state.ispccompiler.builtin_flags .. "--target=" .. scripts.spl_get_variable("build.ispc.target", "") .. " "
    end


    log_dbg("ISPC: Detected executable at: ", state.ispccompiler.path_binary )
end

-- description: inject shader compiler rules for builder
function action.inject_rules(state)
    state.module.build = state.module.build or {}
    state.module.build.variables = state.module.build.variables or {}
    state.module.build.rules = state.module.build.rules or {}
    state.module.build.outputs = state.module.build.outputs or {}
    state.module.build.rules.ispc = state.module.build.rules.ispc or
    {
        command = "${tool_ispc} " .. state.ispccompiler.custom_flags .. " ${flags} ${defines} ${directories} ${in} -o ${out} -h ${outputprefix}${file}.h -M -MF ${out}.d ",
        description = "ISPC ${in}",
        depfile = "${out}.d",
        deps = "gcc"
    }

    state.module.build.variables["tool_ispc"] = state.ispccompiler.path_binary
end

action.get_arch = function(state)
    local arch = tostring(settings.getSetting("build.env.arch", tostring(utils.os_arch))):lower()
    if state ~= nil and state.module.cpp.force_arch ~= nil then arch = state.module.cpp.force_arch:lower() end
    if state ~= nil and state.module.cpp.force_arch == "host" then arch = tostring(utils.os_arch):lower() end
    if arch == "32" or arch == "x32" or arch == "x86" then return "x86" end
    if arch == "64" or arch == "x64" then return "x64" end
    return arch
end

action.get_target = function(state)
    local target =       settings.getSetting("build.env.target", utils.os_name):lower()
    local host_os =      settings.getSetting("build.env.host", utils.os_name):lower()

    -- force overwrites
    if state.module.cpp.force_target ~= nil then
        if state.module.cpp.force_target:lower() == "host" then
            target = host_os
        else
            target = state.module.cpp.force_target:lower()
        end
    end
    return target
end

-- description: generate ispc object file and cpp header from ispc input
function action.generate_build_ispc(state, block, prefix, file, ninputs, defines, blockcppcompiler, blockcpplinker)
    local arch = action.get_arch(state):upper()
    local target = action.get_target(state):upper()
    
    --log_dbg("ISPC: " .. state.module.name .. ": CPP Generate file: " .. file)

    -- generate build
    local extra_defines = table.concat(utils.arr_select(defines, function(x) if x ~= "" then return "-D" .. x end end), " ")
    local outputPrefix = "ispc" .. utils.os_pathsep .. "${mod_basename}" .. utils.os_pathsep
    local ninput = outputPrefix .. file .. ".obj"
    local output = {
        implicit_inputs = { "${tool_ispc}" },
        implicit_outputs = { outputPrefix .. file .. ".h" },
        inputs = { prefix .. file },
        rule = "ispc",
        outputs = { ninput },
        variables = {
            flags = state.ispccompiler.builtin_flags,
            file = file,
            outputprefix = outputPrefix,
            defines = "-DISPC -DISPC_ARCH_" .. arch .. " -DISPC_TARGET_" .. target .. " " .. extra_defines
        }
    }
    table.insert(state.module.build.outputs, output)
    table.insert(ninputs, ninput)

    -- inject into order only inputs for compiler block so that ispc files get generated first.
    blockcppcompiler.order_only_inputs = blockcppcompiler.order_only_inputs or {}
    blockcppcompiler.order_only_inputs = utils.arr_merge(blockcppcompiler.order_only_inputs, output.outputs)
    blockcpplinker.order_only_inputs = blockcpplinker.order_only_inputs or {}
    blockcpplinker.order_only_inputs = utils.arr_merge(blockcpplinker.order_only_inputs, output.outputs)

    -- inject into directories
    blockcppcompiler.directories = blockcppcompiler.directories or {}
    table.insert(blockcppcompiler.directories, "^ispc" .. utils.os_pathsep .. "${mod_basename}")
end


-- description: convert ispc blocks into build outputs
function action.generate_build(state, name, block, blockcppcompiler, blockcpplinker)
    --log_dbg("ISPC: " .. state.module.name .. ": CPP Generate block: " .. name)

    -- handle compiler/linker block in case not exists
    blockcppcompiler = blockcppcompiler or {}
    blockcpplinker = blockcpplinker or {}

    -- handle source files
    local moduleSep = "${module}" .. utils.os_pathsep
    local sourcesFlattened = utils.arr_flatten(block.sources or {})
    local sourcesAbsFlattened = utils.arr_flatten(block.sources_abs or {})
    local definesFlattened = utils.arr_flatten(block.defines or {})

    -- generate preprocessed headers
    local ninputs = {}
    for i, name in pairs(sourcesFlattened) do
        action.generate_build_ispc(state, block, moduleSep, name, ninputs, definesFlattened, blockcppcompiler, blockcpplinker)
    end
    for i, name in pairs(sourcesAbsFlattened) do
        action.generate_build_ispc(state, block, "", name, ninputs, definesFlattened, blockcppcompiler, blockcpplinker)
    end

    -- inject new sources in linker block
    blockcpplinker.sources = utils.arr_merge(blockcpplinker.sources or {}, utils.arr_select(ninputs, function(x) return "^" .. x end) )
end

function action.unload(state)
    log_dbg("ISPC: Unloading..")
end

function action.execute(state, mode)
    log_dbg("ISPC: Executing..")

    action.inject_rules(state)

    for name, block in pairs(state.module.ispc or {}) do
        local blockcppcompiler = state.module.cpp.compiler[name]
        local blockcpplinker = state.module.cpp.linker[name]
        
        action.generate_build(state, name, block, blockcppcompiler, blockcpplinker)
    end
end



return action
